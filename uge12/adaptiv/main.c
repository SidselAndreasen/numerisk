#include<math.h>
#include<assert.h>
#include<stdio.h>
#include<stdlib.h>
double adapt
	(double f(double),double a,double b,double acc,double eps);
double clenshaw_curtis
	(double f(double),double a,double b,double acc,double eps);

int main() //uses gcc nested functions
{

	int calls=0;
	double a=0,b=1,acc=1e-4,eps=1e-4; //grænser og præcisioner
	double s(double x){calls++; return sqrt(x);}; //nested function regner sqrt
	calls=0;
	double Q=adapt(s,a,b,acc,eps);
	double exact=2./3;
	printf("open4: integrating sqrt(x) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));


	calls=0;
	a=0,b=1,acc=1e-4,eps=1e-4;
	double f(double x){calls++; return 1/sqrt(x);}; //nested function
	calls=0;
	Q=adapt(f,a,b,acc,eps);
	exact=2;
	printf("\nopen4: integrating 1/sqrt(x) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	a=0,b=1,acc=0.001,eps=0.001;
	double f2(double x){calls++; return log(x)/sqrt(x);}; //nested function
	calls=0;
	Q=adapt(f2,a,b,acc,eps);
	exact=-4;
	printf("\nopen4: integrating log(x)/sqrt(x) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));


	a=0,b=1,acc=0.001,eps=0.001;
	double f3(double x){calls++; return 4*sqrt(1-pow((1-x),2));}; //nested function
	calls=0;
	Q=adapt(f3,a,b,acc,eps);
	exact=M_PI;
	printf("\nopen4: integrating 4*sqrt(1-(1-x)^2) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));



	//Opgave b
		printf("Opgave B\n");
a=INFINITY,b=INFINITY,acc=0.001,eps=0.001;
calls=0;
double f4(double x){calls++;
if (isinf(b)==1 && isinf(a)==0) {return exp(-x*x/((1+x)*(1+x)))*pow((1+x)*(1+x),-1);}
else if (isinf(b)==1 && isinf(a)==1) {return exp(-x*x/((1-x*x)*(1-x*x)))*(1+x*x)*pow((1-x*x)*(1-x*x),-1);}
else if (isinf(b)==0 && isinf(a)==1) {return exp(-x*x/((1-x)*(1-x)))*pow((1-x)*(1-x),-1);}
else {return exp(-x*x);}
};
double a1;
if (isinf(b)==1 && isinf(a)==0) {return a1=0;}/*
if (isinf(b)==1 && isinf(a)==1) {return a1=-1;} //her går det galt
if (isinf(b)==0 && isinf(a)==1) {return a1=-1;}
else {return a1=0;}
double b1;
if (isinf(b)==1 && isinf(a)==0) {return b1=1;}
if (isinf(b)==1 && isinf(a)==1) {return b1=1;} //her går det galt
if (isinf(b)==0 && isinf(a)==1) {return b1=0;}
else {return b1=0;}*/

Q=adapt(f4,-1,1,acc,eps);
exact=sqrt(M_PI);
printf("open4: integrating exp(-x*x) from %g to %g\n",a,b);
printf("acc=%g eps=%g\n",acc,eps);
printf("              Q = %g\n",Q);
printf("          exact = %g\n",exact);
printf("          calls = %d\n",calls);
printf("estimated error = %g\n",acc+fabs(Q)*eps);
printf("   actual error = %g\n",fabs(Q-exact));


return 0 ;
}
