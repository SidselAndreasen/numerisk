#define pi 3.14159265358979323846264338327950288419716939937510
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_vector.h>

void quasi_random_vector(int dim,double* a,double* b, double* x);
void pseudo_random_vector(int dim,double* a,double* b, double* x, gsl_rng *r);
int plainmc(
	int d, double a[], double b[], double (*func)(double*),
	int N, double* result, double* error);


//double fun(double x[]){return 1/(1-cos(x[0])*cos(x[1])*cos(x[2]))/pi/pi/pi;}
double fun2(double x[]){return x[0]+x[1]*x[2];}
double fun(double x[]){return pow(1-cos(x[0])*cos(x[1])*cos(x[2]),-1)/(M_PI*M_PI*M_PI);}

int main(int argc, char **argv)
{
int N2, N;
//double exact=1.3932039296856768591842462603255;
double exact2=pow(pi,4)*(2+pi)/4;
int d=3; double a[] ={0,0,0}; double b[]={pi,pi,pi};
double result2,error2;
N2=(1e7);

plainmc(d,a,b,&fun2,N2,&result2,&error2);
printf("integrale af x+y*z\n");
printf("-------------- pseudo-random: -------------\n");
printf("N\t= %d\n",N2);
printf("result\t= %g\n",result2);
printf("exact\t= %g\n",exact2);
printf("estimated error\t= %g\n",error2);
printf("actual error\t= %g\n",fabs(result2-exact2));

double exact=1.3932039;
double result,error;
N=(1e7);

plainmc(d,a,b,&fun,N,&result,&error);
printf("integrale af 1/pi^3 * (cos(x)*cos(y)*cos(z))^-1\n");
printf("-------------- pseudo-random: -------------\n");
printf("N\t= %d\n",N);
printf("result\t= %g\n",result);
printf("exact\t= %g\n",exact);
printf("estimated error\t= %g\n",error);
printf("actual error\t= %g\n",fabs(result-exact));
printf("\n\n");

//Opgave b------------------------------
printf("Opgave b\n");
printf("integrale af x+y*z\n");
int m = 1000;
double result3,error3;

for (size_t i = 1; i < m; i++) {
//Sæt forskellige N
//FInd error
plainmc(d,a,b,&fun2,i,&result3,&error3);
printf("%i %g\n",i,error3/(1.0/sqrt(i)));
//find 1/sqrt(N)
}

//printf("N\t= %d\n",N_error(1));

return 0;
}
