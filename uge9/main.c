
#include <gsl/gsl_matrix.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_blas.h>
#define RND ((double)rand()/RAND_MAX)
#define FMT "%7.3f"

int jacobi(gsl_matrix *, gsl_matrix *);
int jacobib(gsl_matrix *);

void printm(gsl_matrix *A){
	for(int r=0;r<A->size1;r++){
		for(int c=0;c<A->size2;c++) printf(FMT,gsl_matrix_get(A,r,c));
		printf("\n");}
}

int main(int argc, char const *argv[]) {
  int n = 7;

if(argc>1) n=atoi(argv[1]);

  gsl_matrix * A = gsl_matrix_calloc(n,n);
for(int i=0;i<n;i++)for(int j=0;j<n;j++){
  gsl_matrix_set(A,i,j,RND);
  double aij = gsl_matrix_get(A,i,j);
  gsl_matrix_set(A,j,i,aij);}
gsl_matrix * V = gsl_matrix_calloc(n,n);
gsl_matrix_set_identity(V);
gsl_matrix * VA = gsl_matrix_calloc(n,n);
if(n>9){printf("n=%i\n",n); return 0;}
printf("A vektoren inden\n");
printm(A);
printf("\n\n");


	gsl_matrix * A_oprindelig = gsl_matrix_calloc(n,n);
	gsl_matrix_memcpy(A_oprindelig,A);
	gsl_matrix * Ab = gsl_matrix_calloc(n,n);
	gsl_matrix_memcpy(Ab,A);
int sweeps=jacobi(A,V); //regne V
printf("sweeps=%i\n",sweeps);
printf("Den diagonaliserede A vektor\n");
printm(A);
printf("\n\n");
printf("V=1*J*J*J osv.\n");
printm(V);
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V,A_oprindelig,0.0,VA);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,VA,V,0.0,A_oprindelig);//V^T A V
printf("\n\n");
printf("D=V^T*A*V\n");
printm(A_oprindelig);
/*printf("\n\n");
int sweepsb=jacobib(Ab);
printf("sweepsb=%i\n",sweepsb);
printf("Opgave b\n");
printf("\n\n");
printm(Ab);*/

//printf("Upper række diagonaliseret\n");
//printm(Ab);


gsl_matrix_free(A); gsl_matrix_free(A_oprindelig); gsl_matrix_free(V); gsl_matrix_free(VA);
}
