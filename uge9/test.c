#include <gsl/gsl_matrix.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_blas.h>
#define RND ((double)rand()/RAND_MAX)
#define FMT "%7.3f"

void printm(gsl_matrix *A){
	for(int r=0;r<A->size1;r++){
		for(int c=0;c<A->size2;c++) printf(FMT,gsl_matrix_get(A,r,c));
		printf("\n");}
}

int main(){
  int changed = 0;
  int n = 7;

  gsl_matrix * A = gsl_matrix_calloc(n,n);
for(int i=0;i<n;i++)for(int j=0;j<n;j++){
  gsl_matrix_set(A,i,j,RND);
  double aij = gsl_matrix_get(A,i,j);
  gsl_matrix_set(A,j,i,aij);}

printm(A);
printf("\n");

  gsl_matrix * J = gsl_matrix_calloc(n,n);
  gsl_matrix * JA = gsl_matrix_calloc(n,n);
  gsl_matrix * V = gsl_matrix_calloc(n,n);
	gsl_matrix * VTAV = gsl_matrix_calloc(n,n);
	gsl_matrix * A_oprindelig = gsl_matrix_calloc(n,n);
  gsl_matrix_set_identity(V);
	gsl_matrix_memcpy(A_oprindelig,A);

//for (size_t k = 0; k < 10; k++) {
do {
	changed=0;
for (int i = 0; i < n; i++) {
for (int j = i+1; j < n; j++) {
  double Aii = gsl_matrix_get(A,i,i), Ajj = gsl_matrix_get(A,j,j);
  double Aij = gsl_matrix_get(A,i,j);
  double phi = atan(Aij/(Ajj-Aii));
  double Aaii = cos(phi)*cos(phi)*Aii-2*sin(phi)*cos(phi)*Aij+sin(phi)*sin(phi)*Ajj;
  double Aajj = sin(phi)*sin(phi)*Aii-2*sin(phi)*cos(phi)*Aij+cos(phi)*cos(phi)*Ajj;
if (Aaii-Aii!=0 || Aajj-Ajj!=0) {
  changed = 1;
  gsl_matrix_set_identity(J);
  gsl_matrix_set(J,i,i,cos(phi)); gsl_matrix_set(J,j,j,cos(phi));
  gsl_matrix_set(J,i,j,sin(phi)); gsl_matrix_set(J,j,i,-sin(phi));

gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,J,A,0.0,JA);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,JA,J,0.0,A);
//printm(A);
//printf("\n");
}
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,V,J,0.0,V);
}}}
while(changed!=0);
//}
printm(A);
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V,A_oprindelig,0.0,VTAV);
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,VTAV,A_oprindelig,0.0,VTAV);
printf("\n\n");
printm(V);
gsl_matrix_free(V); gsl_matrix_free(J); gsl_matrix_free(A); gsl_matrix_free(JA);
gsl_matrix_free(VTAV); gsl_matrix_free(A_oprindelig);
}
