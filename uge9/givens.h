#ifndef HAVE_GIVENS
#define HAVE_GIVENS
struct givens_matrix {int n,p,q;double theta;};
typedef struct givens_matrix givens_matrix;
givens_matrix givens_matrix_transpose(givens_matrix J);
void givens_matrix_left_mult(givens_matrix J,gsl_matrix* A);
void givens_matrix_right_mult(gsl_matrix* A,givens_matrix J);
void givens_matrix_left_multb(givens_matrix J,gsl_matrix* A);
void givens_matrix_right_multb(gsl_matrix* A,givens_matrix J);
#endif
