#include<gsl/gsl_matrix.h>
#include<math.h>
#include"givens.h"

givens_matrix givens_matrix_transpose(givens_matrix J){
	givens_matrix JT = {.n=J.n,.p=J.p,.q=J.q,.theta=-J.theta};
	return JT;
}
//De følgende funktioner bruges til indre produkt af J^T*A*J
//J^T*A
void givens_matrix_left_mult(givens_matrix J,gsl_matrix* A){
	double c=cos(J.theta), s=sin(J.theta);
	int p=J.p,q=J.q;
	for(int i=0;i<J.n;i++){
		double Api= c*gsl_matrix_get(A,p,i)+s*gsl_matrix_get(A,q,i); //Jacobi rotation
		double Aqi=-s*gsl_matrix_get(A,p,i)+c*gsl_matrix_get(A,q,i);
		gsl_matrix_set(A,p,i,Api);
		gsl_matrix_set(A,q,i,Aqi);
	}
}
//(J^T*A)*J)
void givens_matrix_right_mult(gsl_matrix* A,givens_matrix J){
	double c=cos(J.theta), s=sin(J.theta);
	int p=J.p,q=J.q;
	for(int i=0;i<J.n;i++){
		double Aip=c*gsl_matrix_get(A,i,p)-s*gsl_matrix_get(A,i,q); //jacobi rotation
		double Aiq=s*gsl_matrix_get(A,i,p)+c*gsl_matrix_get(A,i,q);
		gsl_matrix_set(A,i,p,Aip);
		gsl_matrix_set(A,i,q,Aiq);

	}
}


//Opgave 2


void givens_matrix_left_multb(givens_matrix J,gsl_matrix* A){
	double c=cos(J.theta), s=sin(J.theta);
	int p=J.p,q=J.q;
	for(int i=0;i<J.n;i++){
		double Api= c*gsl_matrix_get(A,p,i)+s*gsl_matrix_get(A,q,i);
		double Aqi=-s*gsl_matrix_get(A,p,i)+c*gsl_matrix_get(A,q,i);
		gsl_matrix_set(A,p,i,Api);
		gsl_matrix_set(A,q,i,Aqi);
	}
}

void givens_matrix_right_multb (gsl_matrix* A,givens_matrix J){
	double c=cos(J.theta), s=sin(J.theta);
	int p=J.p,q=J.q;
	for(int i=0;i<J.n;i++){
		double Aip=c*gsl_matrix_get(A,i,p)-s*gsl_matrix_get(A,i,q);
		double Aiq=s*gsl_matrix_get(A,i,p)+c*gsl_matrix_get(A,i,q);
		gsl_matrix_set(A,i,p,Aip);
		gsl_matrix_set(A,i,q,Aiq);

	}
}
/*
int main(){
void printm(gsl_matrix *A){
        for(int r=0;r<A->size1;r++){
                for(int c=0;c<A->size2;c++) printf("%9.3g",gsl_matrix_get(A,r,c));
                printf("\n");}
}
	int n=4;
	gsl_matrix* A = gsl_matrix_alloc(n,n);
	gsl_matrix_set_identity(A);
	givens_matrix J={.n=n,.p=0,.q=1,.theta=M_PI/4};
printf("A=\n");
	printm(A);
	givens_matrix_right_mult(A,J);
printf("A=\n");
	printm(A);
return 0;
}
*/
