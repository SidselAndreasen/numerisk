#include <gsl/gsl_matrix.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_blas.h>
#include"givens.h"
int jacobib(gsl_matrix* Ab){
int sweepsb=0, changed;
int n = Ab -> size1;
//gsl_matrix * VT = gsl_matrix_calloc(n,n);
do {
changed=0;
sweepsb++;
for (int p = 0; p < n; p++) {
for (int q = p+1; q < n; q++) {
  double App = gsl_matrix_get(Ab,p,p), Aqq = gsl_matrix_get(Ab,q,q);
  double Apq = gsl_matrix_get(Ab,p,q);
  double phi = 0.5*atan2(2*Apq,Aqq-App);
  double c=cos(phi),s=sin(phi);
  double AApp = c*c*App-2*s*c*Apq+s*s*Aqq;
  double AAqq = s*s*App+2*s*c*Apq+c*c*Aqq;
  if (AApp!=App || AAqq!=Aqq) {
  	changed = 1;

    givens_matrix JTb = {.n=n,.p=p,.q=q,.theta=-phi};
    givens_matrix Jb = {.n=n,.p=p,.q=q,.theta=phi};

//Opgave b
givens_matrix_left_multb(JTb,Ab);
givens_matrix_right_multb(Ab,Jb);


	}
}}}

while(changed!=0);
return sweepsb;
}
