#include <gsl/gsl_matrix.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_blas.h>


void Jacob(gsl_matrix * A){
  int changed = 0;
  int n = A -> size1;

//  gsl_matrix * J = gsl_matrix_calloc(n,n);
//  gsl_matrix * JA = gsl_matrix_calloc(n,n);
//  gsl_matrix * V = gsl_matrix_calloc(n,n);
//	gsl_matrix * VTAV = gsl_matrix_calloc(n,n);
//	gsl_matrix * idiot = gsl_matrix_calloc(n,n);
//	gsl_matrix * V2 = gsl_matrix_calloc(n,n);
//  gsl_matrix_set_identity(V);
//	gsl_matrix_memcpy(A_oprindelig,A);

//for (size_t k = 0; k < 10; k++) {
do {
	changed=0;
for (int i = 0; i < n; i++) {
for (int j = i+1; j < n; j++) {
  double Aii = gsl_matrix_get(A,i,i), Ajj = gsl_matrix_get(A,j,j);
  double Aij = gsl_matrix_get(A,i,j);
  double phi = atan(Aij/(Ajj-Aii));
	double Aaii = cos(phi)*cos(phi)*Aii-2*sin(phi)*cos(phi)*Aij+sin(phi)*sin(phi)*Ajj;
	double Aajj = sin(phi)*sin(phi)*Aii-2*sin(phi)*cos(phi)*Aij+cos(phi)*cos(phi)*Ajj;
	double Aaij = sin(phi)*cos(phi)*(Aii-Ajj)+(cos(phi)*cos(phi)-sin(phi)*sin(phi))*Aij;

//for (size_t k = 0; k < n; k++) {
//	double Aik = gsl_matrix_get(A,i,k), Ajk = gsl_matrix_get(A,j,k);
//	double Aaik = Aik, Aajk = Ajk;

//}

if (Aaii-Aii!=0 || Aajj-Ajj!=0) {
  changed = 1;


//  gsl_matrix_set_identity(J);
//  gsl_matrix_set(J,i,i,cos(phi)); gsl_matrix_set(J,j,j,cos(phi));
//  gsl_matrix_set(J,i,j,sin(phi)); gsl_matrix_set(J,j,i,-sin(phi));

//gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,J,A,0.0,JA);
//gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,JA,J,0.0,A);
//printm(A);
//printf("\n");
int l, k;
for (l = 0; l < n; l++) {
		double Aail = gsl_matrix_get(A,i,l)*cos(phi)-gsl_matrix_get(A,j,l)*sin(phi);
		double Aajl = gsl_matrix_get(A,i,l)*sin(phi)+gsl_matrix_get(A,j,l)*cos(phi);
gsl_matrix_set(A,i,l,Aail);
gsl_matrix_set(A,j,l,Aajl);}
for (k = 0; k < n; k++) {
		double Aaki = gsl_matrix_get(A,k,i)*cos(phi)+gsl_matrix_get(A,k,j)*sin(phi);
		double Aakj = -gsl_matrix_get(A,k,i)*sin(phi)+gsl_matrix_get(A,k,j)*cos(phi);
gsl_matrix_set(A,k,i,Aaki);
gsl_matrix_set(A,k,j,Aakj);}
}
//gsl_matrix_memcpy(V2,V);
//gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,V2,J,0.0,V);
}}}
while(changed!=0);
//}

//gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V2,A_oprindelig,0.0,VTAV);
//gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,VTAV,V2,0.0,idiot);

//gsl_matrix_memcpy(A_oprindelig,idiot);
//gsl_matrix_free(V); gsl_matrix_free(J); gsl_matrix_free(JA);
//gsl_matrix_free(VTAV); gsl_matrix_free(idiot); gsl_matrix_free(V2);
}
