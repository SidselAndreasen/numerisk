#ifndef HAVE_QSPLINE_H
#define HAVE_QSPLINE_H
typedef struct {int n; double *x, *y, *b, *c;} qspline;
qspline* qspline_alloc(int n, double* x, double* y);
double qspline_eval(qspline *s, double z);
double qspline_derivative(qspline *s, double z);
double qspline_integral(qspline *s, double z);
double print_c(qspline *s, int i);
double print_b(qspline *s, int i);
void qspline_free(qspline *s);
#endif
