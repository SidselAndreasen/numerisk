#include <stdlib.h>
#include <math.h>
#include <assert.h>

double qspline_integ(int n, double *x, double *y, double z){


assert(n>1 && z>=x[0] && z<=x[n-1]);
int i=0, j=n-1;
while(j-i>1){
    int m=(i+j)/2;
    if(z>x[m]) i=m;
    else j=m;}

double sum=0;
  for(int k=0; k<i; k++){
  double ak=y[k], bk=(y[k+1]-y[k])/(x[k+1]-x[k]);
//  double integral_1 = 0.5*bk*x[k]*x[k]+ak*x[k], integral_2 = 0.5*bk*x[k+1]*x[k+1]+ak*x[k+1];
  sum += bk/2 * ( (x[k+1]-x[k]) * (x[k+1]-x[k]) ) + ak*(x[k+1]-x[k]);

}
  double b=(y[i+1]-y[i])/(x[i+1]-x[i]), a=y[i];

  sum+=b*((z-x[i])*(z-x[i]))/2+a*(z-x[i]);

  return sum;
}
