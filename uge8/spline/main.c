#include <stdlib.h>
#include <stdio.h>
#define RND (double)rand()/RAND_MAX
#include"qspline.h"
double linterp(int,double*,double*,double);
double linterp_integ(int,double*,double*,double);
#include "cspline.h"

//#include"qspline.h"
//#include"cspline.h"


int main(){
  int n = 10;
  double x[n],y[n];
  for(int i=0;i<n;i++){
    x[i] = i+2 ;
    y[i] = 0.2*i+RND;
    printf("%g %g\n",x[i],y[i]);
  }
  printf("\n\n");
  	qspline* Q = qspline_alloc(n,x,y);
    cubic_spline* C = cubic_spline_alloc(n,x,y);
  double dz = 0.1;

  for(double z=x[0]; z<=x[n-1]; z+=dz){
    double lz = linterp(n,x,y,z);
    double integral = linterp_integ(n,x,y,z);
    double qz=qspline_eval(Q,z);
    double derivative_q = qspline_derivative(Q,z);
    double integral_q = qspline_integral(Q,z);
    double cz = cubic_spline_eval(C,z);
    double derivative_c = cubic_spline_derivative(C,z);
    double integral_c = cubic_spline_integral(C,z);
  printf("%g %g %g %g %g %g %g %g %g\n",z,lz,integral,qz,derivative_q,integral_q,cz,derivative_c, integral_c);
}

cubic_spline_free(C);
qspline_free(Q);
return 0;
}
