#include<matrix.h>
#include<math.h>
#include<assert.h>
#include<stdio.h>
void qr_gs_decomp(matrix A, matrix R){
assert(A.size2==R.size1 && R.size2==R.size1);
for(int i=0;i<A.size2;i++)
	{
	vector ai = matrix_get_column(A,i);
	double Rii = sqrt(vector_dot(ai,ai));
	matrix_set(R,i,i,Rii);
	vector_scale(ai,1/Rii); /* normalisation */
	for(int j=i+1;j<A.size2;j++)
		{
		vector aj = matrix_get_column(A,j);
		double Rij = vector_dot(ai,aj);
		matrix_set(R,i,j,Rij);
		matrix_set(R,j,i,0);
		vector_add(aj,-Rij,ai); /* orthogonalisation */
		}
	}
}
