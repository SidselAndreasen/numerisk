#include<matrix.h>
#include<stdlib.h>
#include<stdio.h>
#define RND (double)rand()/RAND_MAX
void qr_gs_decomp(matrix,matrix);

int main(){
fprintf(stderr,"Garbage collected matrices: DO NOT COPY/PASTE unless you know better\n");
int n=5,m=3;

//vector v = vector_alloc(n);
//for(int i=0;i<v.size;i++)vector_set(v,i,RND);
//vector_print("v=",v);

matrix A = matrix_alloc(n,m);
matrix R = matrix_alloc(m,m);
for(int i=0;i<A.size1;i++)
for(int j=0;j<A.size2;j++)
	matrix_set(A,i,j,RND);
printf("QR-factorisation via Gram-Schmidt orthogonalization\n");
matrix_print("Random matrix A =",A);
matrix Q = matrix_copy(A);
qr_gs_decomp(Q,R);
matrix_print("Q=",Q);
matrix_print("R=(should be right-triangular)",R);
matrix QR=matrix_mm(Q,' ',R,' ');
matrix_print("Q*R= (should be equal the original A)",QR);
matrix QTQ=matrix_mm(Q,'T',Q,' ');
matrix_print("Q^T*Q= (should be equal identity matrix)",QTQ);

return 0;
}
