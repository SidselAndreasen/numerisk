#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#define RND ((double)rand()/RAND_MAX)
#define FMT "%7.3f"

void qrdec(gsl_matrix*, gsl_matrix*);
void qrbak(gsl_matrix*,gsl_matrix*,gsl_vector*,gsl_vector*);

void inverse(gsl_matrix *A, gsl_matrix *B)
{
int n=A->size1;
gsl_matrix *R = gsl_matrix_calloc(n,n);
qrdec(A,R);
gsl_vector *b = gsl_vector_calloc(n);
gsl_vector *x = gsl_vector_calloc(n);
for(int i=0;i<n;i++){
	gsl_vector_set(b,i,1.0);
	qrbak(A,R,b,x);
	gsl_vector_set(b,i,0.0);
	gsl_matrix_set_col(B,i,x);
	}
	gsl_matrix_free(R);
	gsl_vector_free(b);
	gsl_vector_free(x);
}
