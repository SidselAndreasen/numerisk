#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>

void qrbak(gsl_matrix *Q, gsl_matrix *R, gsl_vector *b, gsl_vector *x)
{
	int m=R->size1;
	gsl_blas_dgemv(CblasTrans,1.0,Q,b,0.0,x); // Q^T*b -> x
	for(int i=m-1;i>=0;i--){                  // backsustitution -> x
	double s=0;
	for(int k=i+1;k<m;k++) s+=gsl_matrix_get(R,i,k)*gsl_vector_get(x,k);
	gsl_vector_set(x,i,(gsl_vector_get(x,i)-s)/gsl_matrix_get(R,i,i));
}//forloop med k starter først for k<m. Hvorfor skal i tææle baglæns?
//vi ganger R(i,k) på vector B. Her efter sættes den i'te indgang i x
//b(her kaldet x minus R*b delt med R(i,i)) Det er backsubstitution
}
