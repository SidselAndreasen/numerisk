#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

void qrdec(gsl_matrix* A, gsl_matrix* R)
// QR-decomposition of matrix A, A is replaced with Q, R is filled
{
int m = A->size2;
for(int i=0;i<m;i++){
	gsl_vector_view e = gsl_matrix_column(A,i); //visser alle columns
	double r = gsl_blas_dnrm2(&e.vector); //beregner normen
	gsl_matrix_set(R,i,i,r);
	gsl_vector_scale(&e.vector,1/r); //normalization
	for(int j=i+1;j<m;j++){
		gsl_vector_view q = gsl_matrix_column(A,j);
		double s=0; gsl_blas_ddot(&e.vector,&q.vector,&s);
		gsl_blas_daxpy(-s,&e.vector,&q.vector); //orthogonalization
		gsl_matrix_set(R,i,j,s);
		}
	}
}
