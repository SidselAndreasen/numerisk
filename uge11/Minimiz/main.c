#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<stdio.h>

int newton (
	void f(gsl_vector* x,gsl_vector* fx),
	gsl_vector* x, double dx, double eps);

void vector_print(char* s,gsl_vector* v){
  printf("%s",s);
  for(int i=0;i<v->size;i++)printf("%10.4g ",gsl_vector_get(v,i));
  printf("\n");
  }

int qnewton(double cost(gsl_vector*x), gsl_vector*x, double eps);
void numeric_gradient(double cost(gsl_vector*x),gsl_vector*x, gsl_vector*g);

double rosen(gsl_vector*v){
	double x=gsl_vector_get(v,0);
	double y=gsl_vector_get(v,1);
	return pow(1-x,2)+100*pow(y-x*x,2);
}

double himmel(gsl_vector*v){
	double x=gsl_vector_get(v,0);
	double y=gsl_vector_get(v,1);
	return pow(x*x+y-11,2)+pow(x+y*y-7,2);
}

int main(){
	int n=2,nsteps;
	gsl_vector* x=gsl_vector_alloc(n); //x er vores startgæt-vektor
	gsl_vector* g=gsl_vector_alloc(n);
	double eps=1e-3;
	printf("eps=%.1e\n",eps);

	printf("\nMINIMIZATION OF ROSENBROCK'S FUNCTION\n");
	printf("start point:\n");
	gsl_vector_set(x,0,-2);
	gsl_vector_set(x,1,8);
	gsl_vector_fprintf(stdout,x,"%g"); //print startgæt
	printf("value(start): %g\n",rosen(x)); //værdien af rosen i startpunkt
	nsteps=qnewton(rosen,x,eps);
	printf("steps=%i\n",nsteps);
	printf("minimum found at:\n");
	gsl_vector_fprintf(stdout,x,"%g"); //gemmer minimum i x-vektoren
	printf("value(min): %g\n",rosen(x)); //print funtionsværdierne i minimum
	numeric_gradient(rosen,x,g); //find gradienten i nulpunktet
	printf("gradient(min):\n");
	gsl_vector_fprintf(stdout,g,"%g");

	printf("\nMINIMIZATION OF HIMMELBLAU'S FUNCTION\n");
	printf("start point:\n");
	gsl_vector_set(x,0,-2);
	gsl_vector_set(x,1,8);
	gsl_vector_fprintf(stdout,x,"%g");
	printf("value(start): %g\n",himmel(x));
	nsteps=qnewton(himmel,x,eps);
	printf("steps=%i\n",nsteps);
	printf("minimum found at:\n");
	gsl_vector_fprintf(stdout,x,"%g");
	printf("value(min): %g\n",himmel(x));
	numeric_gradient(himmel,x,g);
	printf("gradient(min):\n");
	gsl_vector_fprintf(stdout,g,"%g");

return 0;
}
