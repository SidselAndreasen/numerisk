#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#define dx (1.0/1048576)

void numeric_gradient(double cost(gsl_vector*x),gsl_vector*x,gsl_vector*gradient){
	double fx=cost(x); //HIMMELBLAU eller ROSENBROCK
	for(int i=0;i<x->size;i++){ //i=0,1
		gsl_vector_set(x,i,gsl_vector_get(x,i)+dx); //x vokser med dx hele tiden
		gsl_vector_set(gradient,i,(cost(x)-fx)/dx); //Vi finder gradienten i alle x. df/dx
		gsl_vector_set(x,i,gsl_vector_get(x,i)-dx); //Du laver x tilbage
		//Minimum er fundet får gradient=0
	}
}

int qnewton(double cost(gsl_vector*x), gsl_vector*x, double eps) {
	int n=x->size,nsteps=0;
	gsl_matrix* B=gsl_matrix_alloc(n,n);
	gsl_matrix_set_identity(B);
	gsl_vector* gradient=gsl_vector_alloc(n);
	gsl_vector* Dx=gsl_vector_alloc(n);
	gsl_vector* z=gsl_vector_alloc(n);
	gsl_vector* gz=gsl_vector_alloc(n);
	gsl_vector* y=gsl_vector_alloc(n);
	gsl_vector* u=gsl_vector_alloc(n);
	numeric_gradient(cost,x,gradient); //find gradient i x
	double fx=cost(x),fz;
	while(1){
		nsteps++;
		gsl_blas_dgemv(CblasNoTrans,-1,B,gradient,0,Dx);//Dx=-1*B*gradient+0*Dx med B=I
		//Vi ender med en vektor Dx=-gradienten
		if(gsl_blas_dnrm2(Dx)<dx)//hvis normen af gradienten er mindre end det lille dx step
			{fprintf(stderr,"qnewton: Dx<dx\n"); break;}//fortæller om if statement er opfyldt
		if(gsl_blas_dnrm2(gradient)<eps)//hvis gradienten er mindre end skridtet stopper vi
			{fprintf(stderr,"qnewton: |grad|<eps\n"); break;}
		int ntrials=0;
		while(1){
			gsl_vector_memcpy(z,x); //gem x i z
			gsl_vector_add(z,Dx); //z=z+Dx=x-gradienten
			fz=cost(z); //bestem funktionen i z
			if(fz<fx)break; //pause
			if(++ntrials>6){//hvis der er taget mere end 6 steps set B til identitet
				gsl_matrix_set_identity(B);
				break;
			}
			gsl_vector_scale(Dx,0.5); //Dx=Dx*0.5
		}
		numeric_gradient(cost,z,gz); //find nye gradient og put i gz
		gsl_vector_memcpy(y,gz); //kopire gz over i y
		gsl_blas_daxpy(-1,gradient,y); //y=-1*gradient+y
		gsl_vector_memcpy(u,Dx); //kopirer Dx over i u
		gsl_blas_dgemv(CblasNoTrans,-1,B,y,1,u); //u=-1*B*y+1*u
		double uy;
		gsl_blas_ddot(y,u,&uy); //uy=y^T*u
		gsl_blas_dger(1.0/uy,u,u,B); //B=1/uy *u*u^T +B
		gsl_vector_memcpy(x,z); //vi kopire z over i x. Vi har fundet en ny x
		gsl_vector_memcpy(gradient,gz); //kopirer gz over i gradienten
		fx=fz; //Vi sætter funktions væredien, lig den for den nye x værdi
	}
gsl_matrix_free(B);
gsl_vector_free(gradient);
gsl_vector_free(Dx);
gsl_vector_free(z);
gsl_vector_free(gz);
gsl_vector_free(y);
gsl_vector_free(u);
return nsteps; //retuner antal steps
}
