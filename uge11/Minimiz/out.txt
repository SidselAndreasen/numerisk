eps=1.0e-03

MINIMIZATION OF ROSENBROCK'S FUNCTION
start point:
-2
8
value(start): 1609
steps=73
minimum found at:
0.999706
0.999412
value(min): 8.63047e-08
gradient(min):
0.000219684
-0.000117021

MINIMIZATION OF HIMMELBLAU'S FUNCTION
start point:
-2
8
value(start): 3026
steps=14
minimum found at:
3.00001
2
value(min): 2.10759e-09
gradient(min):
0.000567547
5.48976e-05
