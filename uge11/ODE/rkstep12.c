#include <stdio.h>

void rkstep12(void f(int n, double x, double*yx, double*dydx),
int n, double x, double* yx, double h, double* yh, double* dy, double* ost){
  double k0[n],yt[n],k12[n],ost1[n],ost2[n];/* VLA: gcc -std=c99 */
  f(n,x    ,yx,k0);  for(int i=0;i<n;i++){yt[i]=yx[i]+ k0[i]*h/2; ost1[i]=yt[i];}
  //Her stepper vi, yt=y_i+1 = yi+k*h/2
  f(n,x+h/2,yt,k12); for(int i=0;i<n;i++){yh[i]=yx[i]+k12[i]*h; ost2[i]=yh[i];}
  //yh=y_i+1=y_i +k12*h
  //Hvor k=f(x,y) vi fandt vores nye dydx ud fra den y, vi fandt oven for
  for(int i=0;i<n;i++) dy[i]=(k0[i]-k12[i])*h/2; //finding error ligning 6
ost[0] = x; ost[1] = ost1[0]; ost[2] = ost2[0];
fprintf(stderr, "%g, %g, %g\n",ost[0],ost[1],ost[2]);
//fprintf(stdout,"ost0=%g,ost1=%g\n",ost[0],ost[1]);

}
