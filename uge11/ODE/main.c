#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int ode_driver(
        void f(int n, double x, double*y, double*dydx),
        int n, double* xlist, double** ylist,
        double b, double h, double acc, double eps, int max);

void f(int n, double x, double* y, double* dydx){
	dydx[0]=y[1];
	dydx[1]=-y[0]; //vores differencial ligning
	return;}

  void f2(int n, double x, double* y, double* dydx){
    double konstant=-10.0;
    dydx[1]=konstant*konstant*y[0];
  	dydx[0]=y[0]*konstant; //vores differencial ligning
//    dydx[1]=y[1]*k
  	return;}

int main(){
  int n=2;
	int max=1000;
	double*xlist=(double*)calloc(max,sizeof(double)); //Vi allocerer plads til en liste med x
	double**ylist=(double**)calloc(max,sizeof(double*)); //og en liste med y
	for(int i=0;i<max;i++) ylist[i]=(double*)calloc(n,sizeof(double));//y laves til matrice n*max
	double pi=atan(1.)*4; //definer pi
	double a=0, b=8*pi, h=0.1, acc=0.01, eps=0.01;
  //RK-matrix, weight, stepsiza, apsolut præcision, relative præcission

	xlist[0]=a; ylist[0][0]=1; ylist[0][1]=0; //start gæt
	int k = ode_driver(f,n,xlist,ylist,b,h,acc,eps,max);
	if(k<0)printf("Opgave B (A er nederst)\n equation: y''=-y\n max steps reached in ode_driver\n");

  for(int i=0;i<k;i++)printf("%g %g\n",xlist[i],ylist[i][0]);
  printf("\n\n");
	for(int i=0;i<k;i++)printf("%g %g\n",xlist[i],cos(xlist[i]));
  printf("\n\n");











//anden funktion
    n=2;
    max=1000;
double*xlist2=(double*)calloc(max,sizeof(double)); //Vi allocerer plads til en liste med x
double**ylist2=(double**)calloc(max,sizeof(double*)); //og en liste med y
for(int i=0;i<max;i++) ylist2[i]=(double*)calloc(n,sizeof(double));//y laves til matrice n*max
a=0, b=2, h=0.1, acc=0.01, eps=0.01;
//RK-matrix, weight, stepsiza, apsolut præcision, relative præcission
xlist2[0]=a; ylist2[0][0]=1; ylist2[0][1]=-10; //start gæt
int k2 = ode_driver(f2,n,xlist2,ylist2,b,h,acc,eps,max);
if(k2<0)printf("Opgave B (A er nederst)\n equation: y'=y*k steps reached in ode_driver\n");
for(int i=0;i<k2;i++)printf("%g %g\n",xlist2[i],ylist2[i][0]);
//printf("# m=1, S=0\n");
printf("\n\n");
//printf("x,sin(x)\n");
for(int i=0;i<k2;i++)printf("%g %g\n",xlist2[i],exp(xlist2[i]*(-10)));
free(xlist); free(ylist);
	return 0;
}
