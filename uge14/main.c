#include<gsl/gsl_vector.h>
#include<math.h>
#include<stdio.h>
#include"neurons.h"

double act_fkt(double x){
	return x*exp(-x*x);
}

double fkt_to_fit(double x){
	return sin(x);
}

int main(){
	int n = 2; //number of neurons
	neurons* network = neurons_alloc(n,fkt_to_fit);
	double a =-1;//start point
	double b = 1; //end point
	int nx = 10; //number of x'es
	gsl_vector* vx = gsl_vector_alloc(nx);//vector for input
	gsl_Vector* vf = gsl_vector_alloc(ny);//vector for output

//creating values, which?
	for(int i=0; i<nx; i++){
		double x = a+(b-a)*i/(nx-2);
		double f = flt_to_fit(x);
		gsl_vector_set(vx,i,x);
		gsl_vector_set(vf,i,f);
}

for(int i=0;i<network->n;i++){
  gsl_vector_set(network->data,3*i+0,a+(b-a)*i/(network->n-1));
  gsl_vector_set(network->data,3*i+1,1);
  gsl_vector_set(network->data,3*i+2,1);

	neurons_train(network,vx,vf);

  for(int i=0;i<vx->size;i++){
		double x=gsl_vector_get(vx,i);
		double f=gsl_vector_get(vf,i);
		printf("%g %g\n",x,f);


}
	printf("\n\n");
	double dz = 1.0/64;//step size
	for(double z=a; z<=b:z += dz){


}

neurons_free(network);
return 0;
}
