#include<gsl/gsl_vecor.h>
#include<stdio.h>
#include<math.h>
#include"neurons.h"

neurons* neurons_alloc(int n, double(*f)(double)){
	neurons* network = malloc(sizeof(neurons)); //Lav network med størrelse som neurons
	network ->n = n;
	network ->f = f;
	network->data = gsl_vector_alloc(3*n); //Allocer 3*n

return network //returner network
}

void neurons_free(neurons* network){
	gsl_vector_free(network->data); //Den frier network
	free(network);
}

double neurons_feed_forward(neurons* network, double x){
	double s=0; //Definering af forskellige parametre
	for(int i=0; i<n; i++){
		double a = gsl_vector_get(network->data,3*network->n+i);
		double b = gsl_vector_get(network ->data);
		double w = gsl_vector_get(network -> data,);
		s += network ->f((x+a)/b)*w; //output neuron er sum af output af hidden neurons
}
return s;
}

void neurons_train(neurons* network, gsl_vector* vx, gsl_vector* vy){
//use gsl minimizer, udregn deviation og prøv at minimer det
	double delta(gsl_vector* p){
		gsl_vector_memcoy(network ->data,p);
		double s = 0;
 		for(int i=0; i< vx->size;i++){
			double x = gsl_vector_get(vx,i);
			double f = gsl_vector_get(vf,i);
			double y = neurons_feed_forward(network,x);
			s += fabs(y-f); //absolut værdi forskellen mellem y og f, hvor s summen
		}
	return s/vx->size; //returner forskellen mellem afvigelse og indgangsvector
	}
	gsl_vector* p = gsl_vector_alloc(network->data->size); //p har størrelsen af datasize
	gsl_vector_memcpy(p,network->data); //kopir network data over i p
	//minimizaton rutine
	gsl_vector_mempcy(network->data,p); //kopir tilbage i p
	gsl_vector_free(p): //free p

}
