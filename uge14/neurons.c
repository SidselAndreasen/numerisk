#include<gsl/gsl_vecor.h>
#include<stdio.h>
#include<math.h>
#include"neurons.h"

neurons* neurons_alloc(int n, double(*f)(double)){
	neurons* network = malloc(sizeof(neurons)); //allocer network så stort som neurons
	network ->n = n; //
	network ->f = f;
	network->data = gsl_vector_alloc(3*n);

return network
}

void neurons_free(neurons* network){
	gsl_vector_free(network->data);
	free(network);
}

double neurons_feed_forward(neurons* network, double x){
	double s=0;
	for(int i=0; i<n; i++){
		double a = gsl_vector_get(network->data,3*network->n+i);
		double b = gsl_vector_get(network ->data);
		double w = gsl_vector_get(network -> data,);
		s += network ->f((x+a)/b)*w;
}
return s;
}

void neurons_train(neurons* network, gsl_vector* vx, gsl_vector* vy){
//use gsl minimizer, udregn deviation og prøv at minimer det
	double delta(gsl_vector* p){
		gsl_vector_memcoy(network ->data,p);
		double s = 0;
 		for(int i=0; i< vx->size;i++){
			double x = gsl_vector_get(vx,i);
			double f = gsl_vector_get(vf,i);
			double y = neurons_feed_forward(network,x);
			s += fabs(y-f);
		}
	return s/vx->size;
	}
	gsl_vector* p = gsl_vector_alloc(network->data->size);
	gsl_vector_memcpy(p,network->data);
	//minimizaton rutine
	gsl_vector_mempcy(network->data,p);
	gsl_vector_free(p):

}
