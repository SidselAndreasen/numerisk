#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<math.h>
#include"ann.h"

ann* ann_alloc(int n,double(*f)(double)){ //tager funktion der afhænger af double??
	ann* network = malloc(sizeof(ann)); //lav network på størrelse af ann
	network->n=n; //definer et n og f under network
	network->f=f;
	network->data=gsl_vector_alloc(3*n); //lav vector tre gange så lang som n
	return network; // returner tre laget neutral network (må være det hidden layer)
}
void ann_free(ann* network){
	gsl_vector_free(network->data); //free den igen
	free(network);
}

double ann_feed_forward(ann* network,double x){
	double s=0;
	for(int i=0;i<network->n;i++){
		double a=gsl_vector_get(network->data,3*i+0); //få den 0 indgang i networkdata
		double b=gsl_vector_get(network->data,3*i+1); //få den 1 indgang i networkdata
		double w=gsl_vector_get(network->data,3*i+2); //få den 2 indgang i networkdata --> tilsammen alle tre gemte neuroner
		s+=network->f((x-a)/b)*w; //Summer af alle y'er, fordi output neuronen er summen af af de tre gemte neuroner
	}
	return s; //returner lle output neuroner
}

int qnewton(double phi(gsl_vector*x), gsl_vector*x, double eps);
int amoeba(int d,double F(double*),double*p, double*h,double size_goal);

void ann_train(ann* network,gsl_vector* vx,gsl_vector* vf){
//Henter netværket indgangsvector og udgangsvektot
	double delta(gsl_vector* p){ //delta funktion tager p
		gsl_vector_memcpy(network->data,p); //putter p over i network
		double s=0;
		for(int i=0;i<vx->size;i++){
			double x=gsl_vector_get(vx,i); //definerer x som vx
			double f=gsl_vector_get(vf,i); //definerer f som vf
			double y=ann_feed_forward(network,x); //Her får vi output neuronen: one big non-linear multi-parameter function y=Fp(x)
			s+=fabs(y-f); //summer alle absolute værdier af output-f (y-f) neuroner og får delta(p)
		}
		return s/vx->size; //Del det med indgangsvektoren.
	}

	gsl_vector* p=gsl_vector_alloc(network->data->size); //Her laves p
	gsl_vector_memcpy(p,network->data); //data kopires over i p
	qnewton(delta,p,1e-3); //quasi-newton rutine minimer
	gsl_vector_memcpy(network->data,p);//p kopires tilbage over i data
	gsl_vector_free(p); //fi p

/*
	double adelta(double* p){
		for(int i=0;i<network->data->size;i++)
			gsl_vector_set(network->data,i,p[i]);
		double s=0;
		for(int i=0;i<vx->size;i++){
			double x=gsl_vector_get(vx,i);
			double f=gsl_vector_get(vf,i);
			double y=ann_feed_forward(network,x);
			s+=fabs(y-f);
		}
		return s/vx->size;
	}
	double p[network->data->size];
	double h[network->data->size];
	double eps=1e-6;
	for(int i=0;i<network->data->size;i++){
		p[i]=gsl_vector_get(network->data,i);
		h[i]=fabs(p[i])/5;
		}
	int nsteps=amoeba(network->data->size,adelta,p,h,eps);
	fprintf(stderr,"train:amoeba:nstpes=%i\n",nsteps);
	for(int i=0;i<network->data->size;i++)
			gsl_vector_set(network->data,i,p[i]);
*/

}
