#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include"ann.h"
double activation_function(double x){return x*exp(-x*x);}//valgt activation function
double function_to_fit(double x){return cos(5*x-1)*exp(-x*x);} //Også en activation function??
int main(){
	int n=5;
	ann* network=ann_alloc(n,activation_function); //??
	double a=-1,b=1; //start og slut punkt
	int nx=20; //data størrelse
	gsl_vector* vx=gsl_vector_alloc(nx); //indgangs vektor
	gsl_vector* vf=gsl_vector_alloc(nx); //Udgang vector
	for(int i=0;i<nx;i++){
		double x=a+(b-a)*i/(nx-1); //input real number
		double f=function_to_fit(x); //x smidt ind i function to fit
		gsl_vector_set(vx,i,x); //set indgangsvectors indgange til x
		gsl_vector_set(vf,i,f); //sæt udgangsvectore til f
	}
	for(int i=0;i<network->n;i++){
		gsl_vector_set(network->data,3*i+0,a+(b-a)*i/(network->n-1));
		gsl_vector_set(network->data,3*i+1,1);
		gsl_vector_set(network->data,3*i+2,1);//valg af network data
	}

	ann_train(network,vx,vf);
  //Minimer a,b og w og plot y som funktio a x.

	for(int i=0;i<vx->size;i++){
		double x=gsl_vector_get(vx,i); //x's indgange sættese til vx
		double f=gsl_vector_get(vf,i); //f's indgange sættes til vf
		printf("%g %g\n",x,f);
	}
	printf("\n\n");

	double dz=1.0/64;
	for(double z=a;z<=b;z+=dz){
		double y=ann_feed_forward(network,z); //Her får vi output neuronerne
		printf("%g %g\n",z,y);
	}

ann_free(network);
return 0;
}
