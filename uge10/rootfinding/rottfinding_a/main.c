#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<math.h>

int newton (
	void f(gsl_vector* x,gsl_vector* fx),
	gsl_vector* x, double dx, double eps);

void vector_print(char* s,gsl_vector* v){
	printf("%s",s);
	for(int i=0;i<v->size;i++)printf("%10.4g ",gsl_vector_get(v,i));
	printf("\n");
	}

int main() {
	int ncalls=0, ncalls_him=0, ncalls_sys=0;
  double A = 10000.;
  void f_sys(gsl_vector* p,gsl_vector* fx){
		ncalls++;
		double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
		gsl_vector_set(fx,0, A*x*y-1); //sæt funktion 1
		gsl_vector_set(fx,1, exp(-x)+exp(-y)-1-1./A); //sæt funktion 2
		}

	void f_ros(gsl_vector* p,gsl_vector* fx){
		ncalls++;
		double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
		gsl_vector_set(fx,0, 2*(1-x)*(-1)+100*2*(y-x*x)*(-1)*2*x); // sæt første afledte
		gsl_vector_set(fx,1, 100*2*(y-x*x)); //sæt anden afledte
		}

  void f_him(gsl_vector* p,gsl_vector* fx){
  	ncalls_him++;
  	double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
  	gsl_vector_set(fx,0, 2*(x*x+y-11)*2*x+2*(x+y*y-7)); //sæt første afledte
  	gsl_vector_set(fx,1, 2*(x*x+y-11)+2*(x+y*y-7)*2*y); //sæt anden afledte
  }
//Ligning system
  gsl_vector* x_sys=gsl_vector_alloc(2);
  gsl_vector_set(x_sys,0,-2);	gsl_vector_set(x_sys,1,8);
  gsl_vector* fx_sys=gsl_vector_alloc(2);
  //Rosenbrock
	gsl_vector* x_ros=gsl_vector_alloc(2);
	gsl_vector_set(x_ros,0,-2);	gsl_vector_set(x_ros,1,8);
	gsl_vector* fx_ros=gsl_vector_alloc(2);
//Himmelblau
  gsl_vector* x_him=gsl_vector_alloc(2);
  gsl_vector_set(x_him,0,-2);	gsl_vector_set(x_him,1,8);
  gsl_vector* fx_him=gsl_vector_alloc(2);

  printf("Root finding:\n");
	printf("extremum of the system of equations's function:\n");
	vector_print("initial vector x: ",x_sys);
	f_ros(x_sys,fx_sys);
	vector_print("            f(x): ",fx_sys);
	newton(f_sys,x_sys,1e-6,1e-3);
	printf("ncalls = %i\n",ncalls_sys);
	//gsl_vector_fprintf(stderr,x_sys,"%g");
	vector_print("      solution x: ",x_sys);
	f_ros(x_sys,fx_sys);
	vector_print("            f(x): ",fx_sys);

	printf("extremum of the Rosenbrock's function:\n");
	vector_print("initial vector x: ",x_ros);
	f_ros(x_ros,fx_ros);
	vector_print("            f(x): ",fx_ros);
	newton(f_ros,x_ros,1e-6,1e-3);
	printf("ncalls = %i\n",ncalls);
	///gsl_vector_fprintf(stderr,x_ros,"%g");
	vector_print("      solution x: ",x_ros);
	f_ros(x_ros,fx_ros);
	vector_print("            f(x): ",fx_ros);

  printf("extremum of the Himmelblau's function:\n");
  vector_print("initial vector x: ",x_him);
  f_him(x_him,fx_him);
  vector_print("            f(x): ",fx_him);
  newton(f_him,x_him,1e-6,1e-3);
  printf("ncalls = %i\n",ncalls_him);
  //gsl_vector_fprintf(stderr,x_him,"%g");
  vector_print("      solution x: ",x_him);
  f_him(x_him,fx_him);
  vector_print("            f(x): ",fx_him);
  gsl_vector_free(x_ros); gsl_vector_free(fx_ros);
  gsl_vector_free(x_him); gsl_vector_free(fx_him);
    gsl_vector_free(x_sys); gsl_vector_free(fx_sys);
}
