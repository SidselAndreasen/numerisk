#ifndef HAVE_QR
#define HAVE_QR
void qrdec  (gsl_matrix* A, gsl_matrix* R);
void qrbak  (gsl_matrix* A, gsl_vector* R);
void qrsolve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b,gsl_vector* x);
void qrinv  (gsl_matrix* Q, gsl_matrix* R, gsl_matrix *B);
#endif
