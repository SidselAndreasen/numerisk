#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>

int newton (
	void f(gsl_vector* x,gsl_vector* fx),
	gsl_vector* x, double dx, double eps);

void vector_print(char* s,gsl_vector* v){
	printf("%s",s);
	for(int i=0;i<v->size;i++)printf("%10.4g ",gsl_vector_get(v,i));
	printf("\n");
	}

int main() {
	int ncalls=0;
  int ncalls2=0;
//rosenbrock
void f(gsl_vector* p,gsl_vector* fx) {
		ncalls++;
		double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
		gsl_vector_set(fx,0, 2*(1-x)*(-1)+100*2*(y-x*x)*(-1)*2*x); //afledte Rosenbrockfunktion for x
		gsl_vector_set(fx,1, 100*2*(y-x*x)); //afledte Rosenbrock i forhold til y
}

//himmelblau
void f2(gsl_vector* p,gsl_vector* fx) {
  	ncalls2++;
  	double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
  	gsl_vector_set(fx,0, 2*(x*x+y-11)*2*x+2*(x+y*y-7));
  	gsl_vector_set(fx,1, 2*(x*x+y-11)+2*(x+y*y-7)*2*y);
}

	gsl_vector* x=gsl_vector_alloc(2);
	gsl_vector_set(x,0,-2);	gsl_vector_set(x,1,8);
	gsl_vector* fx=gsl_vector_alloc(2); //afledte Rosenbrock
  gsl_vector* x2=gsl_vector_alloc(2);
  gsl_vector_set(x2,0,-2);	gsl_vector_set(x2,1,8);
  gsl_vector* fx2=gsl_vector_alloc(2); //afledte Himmelblau

	printf("Root finding:\n");
	printf("extremum of the Rosenbrock's function:\n");
	vector_print("initial vector x: ",x);
	f(x,fx); //her laves fx, ved brug af vektor x
  f2(x2,fx2);
	vector_print("            f(x): ",fx);
  vector_print("            f_2(x): ",fx2);
	newton(f,x,1e-6,1e-3); newton(f2,x2,1e-6,1e-3);
//Rosenbrock
  printf("ncalls = %i\n",ncalls);
	gsl_vector_fprintf(stderr,x,"%g");
	vector_print("      solution x: ",x);
	f(x,fx);
	vector_print("            f(x): ",fx);
//Himmelblau
  printf("ncalls2 = %i\n",ncalls2);
  gsl_vector_fprintf(stderr,x2,"%g");
  vector_print("      solution x2: ",x2);
  f2(x2,fx2);
  vector_print("            f(x)2: ",fx2);

gsl_vector_free(x);
gsl_vector_free(x2);
gsl_vector_free(fx);
gsl_vector_free(fx2);
}
